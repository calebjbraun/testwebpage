#!/usr/bin/python
#Hami Abdi, Simon Orlovsky, & Caleb Braun

''' a class that retrieves a fun fact based on your given information
and specified question, yea! '''

import sys
import password
from decimal import *

import psycopg2
import cgi
import cgitb
cgitb.enable()


class DataSource:

    '''lastName will be a string, ethnicity will be a string, and question
    will be in terms of a number -- for instance, question number 0. There
    are a maximum of 5 question, and an "all questions" default.'''
    def __init__(self, lastName, ethnicity, question):
        # All last names in the database are uppercase
        self.lastName = lastName.upper()
        self.ethnicity = ethnicity
        self.question = question

        # Connect to the database
        db = password.DATABASE_NAME
        usr = password.USER_NAME
        pwd = password.PASSWORD

        try:
            self.connection = psycopg2.connect(database = db, user = usr, password = pwd)
            self.cur = self.connection.cursor()
        except psycopg2.DatabaseError, e:
            print 'Error %s' %e

    def resultOfQuestion0(self):
        '''will retrieve corresponding data to return a list
        containing a string that provides an answer to the question "What
        rank is my last name?"'''
    
        query = 'SELECT * FROM testtable WHERE lastname = \'%s\'' %(self.lastName)
        try:
            self.cur.execute(query)
            row = self.cur.fetchone()
            # The rank of the lastname is the second item of row[] 
            return row[1] 
        except:
            return "Invalid Name"

    def resultOfQuestion1(self):
        '''will retrieve corresponding data to return a list
        containing a string that provides an answer to Question 2:
        "What is my likely ethnicity?'''

        query = 'SELECT * FROM testtable WHERE lastname = \'%s\'' %(self.lastName)

        try:
            self.cur.execute(query)
            row = self.cur.fetchone()
            # There are 6 categories of ethnicities.  The following code
            # makes a list and appends the row data into the list.  The 
            # order is pctwhite, pctblack, pctapi, pctaian, pct2prace,
            # pcthispanic.
            ethnicityList = []
            ethnicityNames = ["White", "Black", "Asian/Pacific Islander", "American Indian", "2 or more races", "Hispanic"]
            for i in range (5, 11):
                ethnicityList.append(row[i])
            # Returns the race at the index of the highest value
            return ethnicityNames[ethnicityList.index(max(ethnicityList))] 
        except:
            return "Invalid Name"

    def resultOfQuestion2(self):
        '''will retrieve corresponding data to return a list
        containing a string that provides an answer to Question 3 '''
        query = 'SELECT prop100k FROM testtable WHERE lastname = \'%s\'' %(self.lastName)
        
        try:
            self.cur.execute(query)
            return float(self.cur.fetchone()[0])
        except:
            return "Invalid Name"

    def resultOfQuestion3(self):
        '''will retrieve corresponding data to return a list
        containing a string that provides an answer to Question 3 '''
        query = 'SELECT lastname FROM testtable ORDER BY rank ASC LIMIT 10'
        popLastNames = []

        self.cur.execute(query)
        rows = self.cur.fetchall()
        for lastnameTuple in rows:
            popLastNames.append(lastnameTuple[0])
        return popLastNames
    
    def resultOfQuestion4(self):
        '''will retrieve corresponding data to return a list
        containing a string that provides an answer to Question 4 '''
        
        query = 'SELECT * FROM testtable WHERE lastname = \'%s\'' %(self.lastName)

        try:
            self.cur.execute(query)
            row = self.cur.fetchone()
            # There are 6 categories of ethnicities.  The following code
            # makes a list and appends the row data into the list.  The 
            # order is pctwhite, pctblack, pctapi, pctaian, pct2prace,
            # pcthispanic.
            ethnicityList = []
            for i in range (5, 11):
                ethnicityList.append(row[i])
            return ethnicityList 
        except:
            return "Invalid Name"



    def resultOfQuestion5(self):
        '''will retrieve corresponding data to return a list
        containing a string that provides an answer to Question 5:
        "How many people in the United States have my last name?'''
        
        query = 'SELECT count FROM testtable WHERE lastname = \'%s\'' %(self.lastName)

        try:
            self.cur.execute(query)
            # Fetches the count cell from the proper last name row.  It is returned as
            # a tuple (of length one) 
            return self.cur.fetchone()[0]
        except:
            return "Invalid Name"

    def resultOfAllDefault(self):
        '''will return a list containing as many strings as there are
        questions to ask, containing the corresponding answer to all of
        them '''
        return ['s', 's', 's', 's', 's']


    def getAnswer(self):
        '''a helper function that will call/implement the correct
        function to retreive the fun fact corresponding to the given
        question. The 'result' will be a list containing the calculated result(s)'''
        result = [self.lastName]
        # What rank is my last name?
        if self.question == 0:
            result.append(self.resultOfQuestion0())

        # What is my likely ethnicity?
        elif self.question == 1:
            result.append(self.resultOfQuestion1())
        
        elif self.question == 2:
            result.append(self.resultOfQuestion2())
            result.append((result[1]/100))
        
        elif self.question == 3:
            result[0] = 'Enter last name'
            result.append(self.resultOfQuestion3())
        
        elif self.question == 4:
            result.append(self.resultOfQuestion4())
        
        elif self.question == 5:
            result.append(self.resultOfQuestion5())
        else:
            result.append(self.resultOfAllDefault())
        # Always gotta close the connection when you're done
        self.connection.close()
        return result
    







