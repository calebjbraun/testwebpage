#!/usr/bin/python
''' webapp.py

    Hami Abdi, Caleb Braun, Simon Orlovosky 
    10/23/14

    A script showing the main page of our Name Game.  Takes in a user's last
    name and prints out facts about it.
'''

import cgi
import cgitb
from DataSource import *
from decimal import *
cgitb.enable()

def sanitizeUserInput(s):
    ''' There are better ways to sanitize input than this, but this is a very
        simple example of the kind of thing you can do to protect your system
        from malicious user input. Unfortunately, this example turns "O'Neill"
        into "ONeill", among other things.
    '''
    charsToRemove = ';,\\/:\'"<>@'
    for ch in charsToRemove:
        s = s.replace(ch, '')
    return s

def getCGIParameters():
    ''' This function grabs the HTTP parameters we care about, sanitizes the
        user input, provides default values for each parameter is no parameter
        is provided by the incoming request, and returns the resulting values
        in a dictionary indexed by the parameter names ('animal' and 'badanimal'
        in this case).
    '''
    form = cgi.FieldStorage()
    parameters = {'name':'', 'question': 0, 'ethnicity':'', 'showsource':''}

    if 'name' in form:
        parameters['name'] = sanitizeUserInput(form['name'].value)

    if 'question' in form:
        parameters['question'] = sanitizeUserInput(form['question'].value)

    if 'ethnicity' in form:
        parameters['ethnicity'] = sanitizeUserInput(form['ethnicity'].value)

    return parameters

def printFileAsPlainText(fileName):
    ''' Prints to standard output the contents of the specified file, preceded
        by a "Content-type: text/plain" HTTP header.
    '''
    text = ''
    try:
        f = open(fileName)
        text = f.read()
        f.close()
    except Exception, e:
        pass

    print 'Content-type: text/plain\r\n\r\n',
    print text

def printMainPageAsHTML(data, question, templateFileName):
    ''' Prints to standard output the main page for this web application, based on
        the specified template file and parameters. The content of the page is
        preceded by a "Content-type: text/html" HTTP header.
        
        Note that this function is quite awkward, since it assumes knowledge of the contents
        of the template (e.g. that the template contains four %s directives), etc. But
        it gives you a hint of the ways you might separate visual display details (i.e. the
        particulars of the HTML found in the template file) from computational results
        (in this case, the strings built up out of animal and badAnimal). 
    '''
    report = ''
    if data[1] == "Invalid Name":
        report = '<p>Sorry! The name you entered is not in our database. Please try again.</p>'
    elif question == 0:
        report = '<p>The name %s has rank %s.</p>\n' % (data[0], data[1])
    elif question ==1:
        report = '<p>Based on the name %s, your likely ethnicity is %s.</p>' %(data[0], data[1])
    elif question == 2:
        report = '<p>In a random sample of 100,000 people, %d people (%.4f%%) would have the last name %s.</p>' %(data[1], data[2], data[0])
    elif question == 3:
        report = '<p>The top 10 most popular names in the United States are: <table align="center"><tr> <td>1</td><td>%s</td></tr><tr> <td>2</td><td>%s</td></tr><tr> <td>3</td><td>%s</td></tr><tr> <td>4</td><td>%s</td></tr><tr> <td>5</td><td>%s</td></tr><tr> <td>6</td><td>%s</td></tr><tr> <td>7</td><td>%s</td></tr><tr> <td>8</td><td>%s</td></tr><tr> <td>9</td><td>%s</td></tr><tr> <td>10</td><td>%s</td></tr></table></p>' %(data[1][0], data[1][1], data[1][2], data[1][3], data[1][4], data[1][5], data[1][6], data[1][7], data[1][8], data[1][9])
    elif question == 4:
        report = '<p>Here is the ethnicity breakdown of the name %s: <br> White: %0.2f <br> Black: %0.2f <br> Asian/Pacific Islander: %0.2f <br> American Indian/Alaskan Native: %0.2f <br> Non-Hispanic of two or more races: %0.2f <br> Hispanic: %0.2f</p>' %(data[0], data[1][0], data[1][1], data[1][2], data[1][3], data[1][4], data[1][5])
    elif question == 5:
        report = '<p>There are %s people in the United States with the name %s.</p>' %(data[1], data[0])
    else:
        report = '<p> Please enter ethnicity and last name</p>'

    links = '<p><a href="DataDescription.html">About the Data</p>'

    outputText = ''
    try:
        f = open(templateFileName)
        templateText = f.read()
        f.close()
        outputText = templateText % (data[0], data[0], data[0], report, links)
    except Exception, e:
        outputText = 'Cannot read template file "%s".' % (templateFileName)

    print 'Content-type: text/html\r\n\r\n',
    print outputText

def main():
    parameters = getCGIParameters()
    getData = DataSource(parameters['name'], parameters['ethnicity'], int(parameters['question']))
    data = getData.getAnswer()
    printMainPageAsHTML(data, int(parameters['question']), 'template.html')
        
if __name__ == '__main__':
    main()


